
drop database participante_24;

create database participante_24;
use participante_24;

create table cliente(
	id_cliente int primary key auto_increment,
    nombre varchar(150),
    apellido varchar(150),
    direccion varchar(200),
    fecha_nacimiento date,
    telefono int,
    email varchar(200)
)Engine InnoDB;

create table categoria(
	id_categoria int primary key auto_increment,
    nombre varchar(150),
    descripcion varchar(200)
)Engine InnoDB;

create table producto(
	id_producto int primary key auto_increment,
    nombre varchar(150),
    precio double (10,2),
    stock int,
    id_categoria int,
    foreign key (id_categoria) references categoria(id_categoria) on delete cascade on update cascade
)Engine InnoDB;

create table modo_pago(
	num_pago int primary key auto_increment, 
    nombre varchar(150),
    otros_detalles varchar(200)
)Engine InnoDB;

create table factura(
	num_factura int primary key,
    id_cliente int,
    fecha date,
    num_pago int,
    foreign key (id_cliente) references cliente(id_cliente) on delete cascade on update cascade,
    foreign key (num_pago) references modo_pago(num_pago) on delete cascade on update cascade
)Engine InnoDB;

create table detalle(
	num_detalle int primary key auto_increment,
    id_factura int,
    id_producto int,
    cantidad int,
    precio double(10,2),
    
    foreign key (id_producto) references producto(id_producto) on delete cascade on update cascade,
    foreign key (id_factura) references factura(num_factura) on delete cascade on update cascade    
)Engine InnoDB;


DELIMITER //
CREATE PROCEDURE sp_insercion_factura(in id_cliente_p int, in fecha_p date,  in num_pago_p int, in id_producto_p int, in cantidad_p int)

BEGIN
	DECLARE num_fact_v int; 
	DECLARE num_det_v int;
    DECLARE precio_v double(10,2);
    
    SELECT precio FROM producto WHERE id_producto = id_producto_v INTO precio_v;
    
	SELECT num_factura + 1 FROM factura INTO num_fact_v;     
    INSERT INTO factura VALUES (num_fact_v, id_cliente_p, fecha_p, num_pago_p); 
    
      
	SELECT num_detalle + 1 FROM detalle INTO num_det_v; 
    INSERT INTO detalle VALUES(nom_det_v, num_fact_v, id_producto_v, cantidad_v, precio_v);

END;
DELIMITER //

DELIMITER //
CREATE TRIGGER tr_disminuye_stock AFTER INSERT ON detalle FOR EACH ROW
BEGIN
	
    UPDATE producto SET stock = stock -  NEW.cantidad;
    
END;
DELIMITER //